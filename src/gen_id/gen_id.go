/* gen_id: Generates a list of IDs based on the input format.

    ./gen_id --format LLDDLL[abc] -f file.csv -n 1000
    ./gen_id --help

    Author: Victor Petrov <victor_petrov@harvard.edu>

   (c) 2014 The Neuroinformatics Research Group at Harvard University
   (c) 2014 The President and Fellows of Harvard College
*/

package main

import (
	"flag"
	"log"
	"math/rand"
	"os"
	"time"
)

const (
	DEFAULT_COUNT = 1
)

var (
	//all letters, except a few that might be confused for numbers
	LETTERS []rune = []rune("ABCDEFGHJKLMNPQRSTUVWXYZ")
	DIGITS  []rune = []rune("0123456789")

	//program arguments
	idFormat       string
	idCount        int
	outputFilePath string
)

//generates idCount number of IDs and writes them to a file
func main() {
	var (
		i    int = 0
		id   string
		ids  []string = make([]string, 0)
		done bool
	)

	//seed the random number generator using the current number of nanoseconds
	//since the Epoch
	rand.Seed(time.Now().UnixNano())
	ParseArguments()

	//loop until we have enough IDs
	for i < idCount {
		//generate an id
		id, done = genID(idFormat)

		//add to the list of IDs only if it doesn't already exist
		if !containsID(ids, id) {
			ids = append(ids, id)
			i++
		}

		if done {
			log.Printf("WARNING: The format '%v' cannot be used to generate %v distinct IDs.", idFormat, idCount)
			break
		}
	}

	//save list to file
	err := writeListToFile(ids, outputFilePath)
	if err != nil {
		log.Fatalf("%v: %v", outputFilePath, err)
	}
}

//outputs a string list to a file path. returns any errors encountered.
func writeListToFile(l []string, filename string) (err error) {
	var f *os.File

	//if the filename is "-", use stdout, otherwise, open a file
	if filename != "-" {
		f, err = os.Create(filename)
		if err != nil {
			return err
		}
		defer f.Close()
	} else {
		f = os.Stdout
	}

	//loop over all strings and write them to the file
	for _, id := range l {
		_, err = f.WriteString(id + "\n")
		if err != nil {
			break
		}
	}

	return err
}

//generate an ID based on the format argument.
//The format has the following syntax: ([[::print::]]|L|D|[character*])*
//i.e. one or more letters, digits or characters from a given list
//only 1 character is chosen at random from the character list
// [[::print::]]: any printable character, except [ and ]
//  L: an uppercase letter
//  D: a digit
//  []: a character list, e.g. [AB123], [Z]
//
// Caveat: There is currently no way to print a '[' or ']' characters, as
// they are special and this function does not implement escaping.
// Also, it doesn't implement character ranges, such as 0-9, A-Z.
//
// Example format string: heLLo[123]Lz1 -> heXYo2Az1
//
//If this function returns done=true, then the format will always generate
//the same string
func genID(format string) (id string, done bool) {
	var (
		result          []rune = make([]rune, 0)
		runeList        []rune = make([]rune, 0)
		runeListStarted bool   = false
		lastListStart   int    = -1
	)

	done = true

	//loop over each character in the
	for i, r := range format {
		switch r {
		case 'L':
			result = append(result, randomRuneFrom(LETTERS))
			done = false
			break
		case 'D':
			result = append(result, randomRuneFrom(DIGITS))
			done = false
			break
		//parse a character list [ABCD]
		case '[':
			if runeListStarted {
				log.Fatalf("Error in format string: there is an unexpected list character ('[') at character  #%v. "+
					"Make sure it's closed with ']' before you start a new list.", lastListStart+1)
			}
			runeListStarted = true
			lastListStart = i
			runeList = nil
			break
		case ']':
			if !runeListStarted {
				log.Fatalf("Error in format string: there is an unexpected list closing character (']') at position #%v. "+
					"A list can be opened with the '[' character.", i+1)
			}

			runeListStarted = false
			if len(runeList) > 0 {
				result = append(result, randomRuneFrom(runeList))
				done = false
			}
			break
		default:
			if runeListStarted {
				if !containsRune(runeList, r) {
					runeList = append(runeList, r)
				}
			} else {
				result = append(result, r)
			}
			break
		}
	}

	id = string(result)
	return
}

//returns a random rune from the list passed as a parameter
func randomRuneFrom(l []rune) rune {
	r := rand.Int()
	nrunes := len(l)

	if nrunes == 0 {
		log.Fatalln("Error in format string: a character list must not be empty")
	}

	r = r % nrunes

	return l[r]
}

//parses command line arguments by using global variables
func ParseArguments() {
	flag.StringVar(&outputFilePath, "f", "IDs.csv", "Path to output file (default:'IDs.csv', use '-' to print)")
	flag.StringVar(&idFormat, "format", "", "The format of IDs to generate (L-letter, D-digit, [ABC123]-either; e.g. -format 'LLDDL[AB12]'")
	flag.IntVar(&idCount, "n", DEFAULT_COUNT, "Number of IDs to create")
	flag.Parse()
}

//return true if a list of runes contains the specified rune, or false otherwise
func containsRune(l []rune, r rune) bool {
	for _, x := range l {
		if x == r {
			return true
		}
	}

	return false
}

//returns tru eif a list of strings contains the specified string, or false otherwise
func containsID(l []string, id string) bool {
	for _, x := range l {
		if x == id {
			return true
		}
	}

	return false
}
